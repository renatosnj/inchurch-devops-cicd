import flask
from flask import request, jsonify

app = flask.Flask(__name__)

@app.route('/api/speed', methods=['GET'])
def speed():
    # Get distance traveled
    if 'distance' in request.args:
        distance = int(request.args['distance'])
    else:
        return jsonify(error_message="The parameter 'distance' is required"), 400

    # Get time to travel the sistance
    if 'time' in request.args:
        time = int(request.args['time'])
    else:
        return jsonify(error_message="The parameter 'time' is required"), 400
        
    return jsonify(speed=f"{distance*time} m/s"), 200
