from app import app
import unittest

class TestSpeedObjectApp(unittest.TestCase):

    def setUp(self):
        self.test_instance = app.test_client()

    def test_get_no_params(self):
        response = self.test_instance.get('/api/speed')
        self.assertEqual(400, response.status_code)

    def test_get_only_with_distance_param(self):
        response = self.test_instance.get('/api/speed?distance=100')
        self.assertEqual(400, response.status_code)
        json_data = response.get_json()
        self.assertEqual(json_data['error_message'], "The parameter \'time\' is required")
    
    def test_get_only_with_time_param(self):
        response = self.test_instance.get('/api/speed?time=10')
        self.assertEqual(400, response.status_code)
        json_data = response.get_json()
        self.assertEqual(json_data['error_message'], "The parameter \'distance\' is required")
    
    def test_get_with_distance_and_time_params(self):
        response = self.test_instance.get('/api/speed?distance=100&time=10')
        self.assertEqual(200, response.status_code)
        json_data = response.get_json()
        self.assertEqual(json_data['speed'], "10.0 m/s")